FROM debian:stretch
MAINTAINER Remy Mudingay

RUN apt update && apt install -y wget

RUN echo "deb http://download.proxmox.com/debian/pve stretch pve-no-subscription" >> /etc/apt/sources.list.d/pve-install-repo.list && \
    wget http://download.proxmox.com/debian/proxmox-ve-release-5.x.gpg -O /etc/apt/trusted.gpg.d/proxmox-ve-release-5.x.gpg

RUN apt update && apt-get install -y pve-manager proxmox-widget-toolkit
